const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("Our first unit tests", () => {
    before(() => {
        // Initialization
        // create objects... etc...
        console.log("Initialising tests");
    });
    it("Can add 1 and 2 together", () => {
        // Tests 
        expect(mylib.add(1,2)).equal(3, "1 + 2 is not 3, for some reason?");
    });
    after(() => {
        // Cleanup
        // For example: shutdown the Express server.
        console.log("Testing completed!")
    });
});